define ['libUnderScore'], () ->
	timer = 0
	xhr = {readyState: 0}
	st =
		body: 'body'
		ctnLoader: '.box_search'
		selectContent: '.distric'
		tplEmtySearch: '.tplDistrict'
	dom = {}
	catchDom = (st) ->
		dom.body= $(st.body)
		dom.ctnLoader = $(st.ctnLoader)
		dom.selectContent = $(st.selectContent)
		dom.tplEmtySearch = _.template $(st.tplEmtySearch).html()
		return
	suscribeEvents = ->
		functions.searchMovie()
		# $(window).resize functions.resizeWindow
		return
	events =
		evtSearch: ()->
			return
	functions =
		searchMovie : ()->
			$.ajax
				method: "GET"
				url: '/wservice-client/ubigeo'
				dataType: "JSON"
				beforeSend: ()->
					dom.selectContent.html('')
					return
				success: (response)->
					if response.status is 1
						html = dom.tplEmtySearch
							result: response.data
					dom.selectContent.html html
					return
			return
	catchDom(st)
	suscribeEvents()
	return