define ['libHighCharts'], (Morris,Raphael) ->
	console.log 'showCharts...'
	st =
		body : 'body, html'
		barChart: '.barChart'
	dom = {}
	days = []
	keys = Object.keys
	catchDom = ->
		dom.body = $(st.body)
		dom.barChart = $(st.barChart)
		return
	catchDom()
	functions =
		showCharts: ()->
			keys(dataServices.Servicios).forEach (item, idx)->
				days.push(keys(dataServices.Servicios[item])[0])
			$.each dom.barChart,()->
				$this = $(this)
				label = $this.attr 'data-label'
				variable = $this.attr 'data-var'
				$this.highcharts
					chart:
						type: 'column'
						backgroundColor: '#ffffff'
						plotBlorderColor: '#435860'
					title:
						text: label
						style:
							color:'#435860'
							fontSize:'20'
					xAxis:
						categories: days
						type: 'category'
						labels:
							style:
								color:'#435860'
					yAxis:
						min:0
						title:
							text: '# de '+label
							style:
								color:'#435860'
						labels:
							style:
								color:'#435860'
					legend: enabled: false
					series: [{
						name:'Número de ' + label
						data: data[variable]
						}]
			$('text:contains("Highcharts.com")').remove()
	functions.showCharts()
	return