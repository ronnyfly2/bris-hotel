define(['datatables'], () ->
	console.log 'initDatatables...'
	st =
		table: '.js-datatable'
	dom = {}
	window.table = {}
	catchDom = ()->
		dom.table= $(st.table)
		return
	functions =
		makeTables: ()->
			if dom.table.is(":visible")
				columns= dom.table.attr "data-cols"
				url =  dom.table.attr "data-url"
				id = dom.table.attr "id" 
				array= columns.split ","
				obj= []
				i= 0
				for key of array
					obj.push
						data: array[key]
				filtro = ""
				filtroVal = ""
				if $('.js-filter').length > 0
					filtro = $('.js-filter').attr("data-filter")
					filtroVal = $('.js-filter').val().toString()
				window.table[id] = dom.table.DataTable
					oLanguage:
						"sUrl": "//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json"
					processing: true
					serverSide: true
					ordering: false
					searching: true
					bLengthChange: false
					ajax:
						"url": url+"?"+filtro+"="+filtroVal
					columns: obj
				$('.search').keyup ()->
					window.table[id].search($(this).val()).draw()
				if $('.js-filter').length > 0
					$('.js-filter').on 'change', ()->
						filterUrl = url+"?"+filtro+"="+$('.js-filter').val().toString()
						window.table[id].ajax.url( filterUrl ).load()
			return
		showStars: ()->
			if (alpha.controller == "Provider") && (alpha.action == "getIndex")
				window.table['category'].on 'draw', ()->
					$('.stars').each ()->
						$this = $(this)
						val = parseInt($this.html())
						i = 0
						starsHtml = ""
						if val == 0
							starsHtml = "<i class='icon icon-star'></i>"
						else
							while i < val
								i++
								starsHtml += "<i class='icon icon-star active'></i>"
						$this.html starsHtml
					return
			return
	initialize = ->
		catchDom()
		functions.makeTables()
		functions.showStars()
		return
	return {
		init: initialize()
	}
)