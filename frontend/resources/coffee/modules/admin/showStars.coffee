define [], () ->
	console.log 'showStars...'
	st =
		table: 'table'
	dom = {}
	window.table = {}
	catchDom = ()->
		dom.table= $(st.table)
		return
	functions =
		showStars: ()->
			window.table['category'].on 'draw', ()->
				$('.stars').each ()->
					$this = $(this)
					val = parseInt($this.html())
					i = 0
					starsHtml = ""
					if val == 0
						starsHtml = "<i class='icon icon-star'></i>"
					else
						while i < val
							i++
							starsHtml += "<i class='icon icon-star active'></i>"
					$this.html starsHtml
				return
			return
	initialize = ->
		catchDom()
		functions.showStars()
		return
	return {
		init: initialize()
	}
