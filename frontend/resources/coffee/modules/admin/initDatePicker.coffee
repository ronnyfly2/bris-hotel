define(['libDatePicker'], () ->
	jQuery.datetimepicker.setLocale('es');
	console.log 'initDatepicker...'
	st =
		body : 'body, html'
		datepicker: '.js-datepicker'
	dom = {}
	catchDom = ->
		dom.body = $(st.body)
		dom.datepicker = $(st.datepicker)
	catchDom()
	data = {}
	functions =
		datepickerBind: ()->
			dom.datepicker.each ()->
				$this = $(this)
				if $this.attr('data-range') == 'from'
					$this.datetimepicker
						closeOnDateSelect: true
						onShow: ()->
							$to = $('.js-datepicker[data-range="to"]')
							this.setOptions
								maxDate: if $to.val() then $to.val() else false
				else if $this.attr('data-range') == 'to'
					$this.datetimepicker
						closeOnDateSelect: true
						onShow: ()->
							$from = $('.js-datepicker[data-range="from"]')
							this.setOptions
								minDate: if $from.val() then $from.val() else false
				else
					$this.datetimepicker()
			return
	functions.datepickerBind()
	return
)