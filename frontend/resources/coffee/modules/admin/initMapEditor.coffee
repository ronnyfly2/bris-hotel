define ['libFancybox'], () ->
	console.log "showMap loader..."
	st =
		latitud: '#lat'
		longitud: '#lng'
	dom = {}
	map = null
	myLatLng = {}
	catchDom = ->
		dom.latitud = $(st.latitud)
		dom.longitud = $(st.longitud)
		return
	catchDom()

	functions =
		getInitialPin: ()->
			if (dom.latitud.val() != "" ) || (dom.longitud.val() != "")
				myLatLng.lat = parseFloat(dom.latitud.val())
				myLatLng.lng = parseFloat(dom.longitud.val())
				# myLatLng.lat = parseFloat("-12.046353")
				# myLatLng.lng = parseFloat("-77.042737")
			else
				myLatLng.lat = parseFloat("-12.046353")
				myLatLng.lng = parseFloat("-77.042737")
			return
		showMapEditor: ()->
			functions.getInitialPin()
			functions.initMap()
			return
		initMap: ()->
			functions.mapSettings()
			functions.addMarker()
			return
		mapSettings: ()->
			mapProp = 
				center: myLatLng
				zoom: 16
				mapTypeId: google.maps.MapTypeId.ROADMAP
				streetViewControl: false
			map = new google.maps.Map(document.getElementById("mapId"), mapProp)
			return
		addMarker: ()->
			marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				icon: '../../../img/pin.png'
				draggable:true
				})
			google.maps.event.addListener marker, 'dragend', (event) ->
				document.getElementById('lat').value = @getPosition().lat()
				document.getElementById('lng').value = @getPosition().lng()
				return
	window.functions = functions
	functions.showMapEditor()
	return

window.mapSettings= ()->
	console.log "mapSettings"
	map = new (google.maps.Map)(document.getElementById('mapId'),
		center:
			lat: -34.397
			lng: 150.644
		zoom: 8)
	return
window.initMap= ()->
	console.log "entra initMap"
	google.maps.event.addDomListener(window, 'load', window.mapSettings)
	return