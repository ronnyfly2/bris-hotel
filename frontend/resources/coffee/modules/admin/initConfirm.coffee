define(['libJqueryConfirm','libJqUtils'], () ->
	console.log 'initConfirm...'
	st =
		body : 'body, html'
		btnDeleteAction : '.js-delete-confirm'
		btnChangeAction : '.js-change-confirm'
		btnPremiumAction : '.js-premium-confirm'
	dom = {}
	catchDom = ->
		dom.body = $(st.body)
		dom.btnDeleteAction = $(st.btnDeleteAction)
		dom.btnChangeAction = $(st.btnChangeAction)
		dom.btnPremiumAction = $(st.btnPremiumAction)
		return
	catchDom()
	data = {}
	functions =
		premiumConfirm: ()->
			$('table').on 'click', st.btnPremiumAction, ()->
				$this = $(this)
				data= {}
				data.title = 'Cambiar estado'
				data.content = '¿Desea cambiar de estado al registro?'
				data.confirmBtn = 'Cambiar'
				data.cancelBtn = 'Cancelar'
				data.contentSuccess = 'Se ha cambiado el estado del registro correctamente'
				data.contentError = 'Ha ocurrido un error, intentelo nuevamente'
				data.obj = $(this)
				functions.confirmMessage data
				return
		deleteConfirm: ()->
			$('table').on 'click', st.btnDeleteAction, ()->
				$this = $(this)
				data= {}
				data.title = 'Eliminar'
				data.content = '¿Desea eliminar el registro?'
				data.confirmBtn = 'Eliminar'
				data.cancelBtn = 'Cancelar'
				data.contentSuccess = 'Se ha eliminado el registro correctamente'
				data.contentError = 'Ha ocurrido un error, intentelo nuevamente'
				data.obj = $(this)
				functions.confirmMessage data
		changeStatusConfirm: ()->
			$('table').on 'click', st.btnChangeAction, ()->
				$this = $(this)
				data= {}
				data.title = 'Cambiar estado'
				data.content = '¿Desea cambiar el estado del registro?'
				data.confirmBtn = 'Cambiar'
				data.cancelBtn = 'Cancelar'
				data.contentSuccess = 'Se ha cambiado el estado del registro correctamente'
				data.contentError = 'Ha ocurrido un error, intentelo nuevamente'
				data.obj = $(this)
				functions.confirmMessage data
		confirmMessage: (data)->
			$this = data.obj
			$.confirm
				title: data.title
				content: data.content
				confirmButton: data.confirmBtn
				cancelButton: data.cancelBtn
				confirmButtonClass: 'btn-primary'
				cancelButtonClass: 'btn-danger'
				theme: 'hololight'
				closeIcon:true
				confirm: ()->
					route = $this.attr 'data-url'
					utils.loader $('.container_form'), true
					$.ajax
						url: route
						success: (dataResponse)->
							if dataResponse.state == 1
								$.dialog
									title: 'Correcto'
									content: data.contentSuccess
									onOpen: ()->
										$('.glyphicon.glyphicon-remove').removeClass().addClass('icon icon-close')
										return
									id = $('table').attr 'id'
									window.table[id].draw()
								utils.loader $('.container_form'), false
							else
								dataResponse.msg
								$.dialog
									title: 'Error'
									content: data.contentError
									onOpen: ()->
										$('.glyphicon.glyphicon-remove').removeClass().addClass('icon icon-close')
										return
								utils.loader $('.container_form'), false
						error:()->
							$.dialog
								title: 'Error'
								content: data.contentError
							utils.loader $('.container_form'), false
				onOpen: ()->
					$('.glyphicon.glyphicon-remove').removeClass().addClass('icon icon-close')
					return
				cancel: ()->
					return
		simpleAlert: ()->
			text = $('.alert li').html()
			if text != '' && text != undefined
				title = ''
				if $('.alert').attr('class').search('success') >= 0
					title = "Correcto"
				else if $('.alert').attr('class').search('warning') >= 0
					title = "Alerta"
				$('.alert').remove()
				$.confirm
					title: title
					content: text
					cancelButton: false
					autoClose: 'confirm|3000'
					confirmButton: 'Continuar'
					onOpen: ()->
						console.log 'removeeeeeee2'
						$('.glyphicon.glyphicon-remove').removeClass().addClass('icon icon-close')
			return
	functions.deleteConfirm()
	functions.changeStatusConfirm()
	functions.simpleAlert()
	functions.premiumConfirm()
	return
)