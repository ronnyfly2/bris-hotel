define(['libParsley','libJqUtils'], () ->
	console.log 'getAuth...'
	st =
		contentFormLogin	: 'form.login'
		contentFormRecover	: 'form.recover'
		submitLogin			: '.btn_login'
		submitRecover		: '#resetPassword button'
		domainData			: window.location
	dom = {}
	catchDom = ->
		dom.contentFormLogin	= $(st.contentFormLogin)
		dom.contentFormRecover	= $(st.contentFormRecover)
		dom.submitLogin			= $(st.submitLogin)
		dom.submitRecover		= $(st.submitRecover)
		return
	suscribeEvents = ->
		dom.submitLogin.on 'click', events.getParsleyLogin
		$('input').on 'keyup', events.pressEnter
		dom.submitRecover.on 'click', events.getParsleyRecover
		return
	events =
		getParsleyLogin:(e)->
			dom.contentFormLogin.parsley().validate()
			if dom.contentFormLogin.parsley().isValid()
				events.getAjaxLogin()
				return
			return
		getParsleyRecover:(e)->
			dom.contentFormRecover.parsley().validate()
			if dom.contentFormRecover.parsley().isValid()
				events.getAjaxRecover()
				e.preventDefault()
				return
			return
		getAjaxLogin : ()->
			$.ajax
				method: "POST"
				url: 'login' 
				dataType: "JSON"
				data:
					"email":$('.email').val()
					"password":$('.pass').val()
					"tokendevice":$('.token').val()
					"typedevice":1
				beforeSend: ()->
					$('.alert').removeClass('actived')
					utils.loader $('.box_login'), true
					return
				success: (response)->
					if response.state == 1
						linkUrl = st.domainData.origin
						redirection = response.data.redirect
						urlRedirect = linkUrl + redirection
						document.location.href = urlRedirect
						return
					else
						utils.loader $('.box_login'), false
						$('.alert_message').addClass('actived')
						$('.alert_message ul li').text(response.data_error.email)
						return
					return
			return
		getAjaxRecover:()->
			utils.loader $('.box_login'), true
			emailData = decodeURIComponent($('.reco_email').val())
			$.ajax
				method: "GET"
				url: "resetPassword?email=#{emailData}"
				success: (response) ->
					utils.loader $('.box_login'), false
					message = response.message
					status = response.status
					$('.message').show('slow').text(response.message)
			return
		pressEnter:(e)->
			if e.which == 13
				e.preventDefault()
				events.getParsleyLogin()
				events.getParsleyRecover()
				return
	catchDom()
	suscribeEvents()
	return
)