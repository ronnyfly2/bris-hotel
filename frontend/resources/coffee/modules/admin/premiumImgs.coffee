define(['libBootstrapUpload','libJqUtils'], () ->
	#	For a single simple upload main 
	#	image add the classes "js-main-img js-fileinput"
	console.log 'premiumImgs...'
	st =
		body : 'body, html'
		select: '.js-premium-cbo'
		ctn: '.img-ctn'
		input: '.js-fileinput' #main class for bind
		tpl: '#tplImgInput'
		mainImg: '.js-main-img' #must parent class for single required img
	dom = {}
	catchDom = ->
		dom.body = $(st.body)
		dom.select = $(st.select)
		dom.ctn = $(st.ctn)
		dom.input = $(st.input)
		dom.tpl = $(st.tpl).html()
		dom.mainImg = $(st.mainImg)
	catchDom()
	data = {}
	fileInputPreview = {}
	functions =
		bindSimple: ()->
			dom.input.each ()->
				$this = $(this)
				if $this.parents('.img-ctn').length == 0
					utils.loader($('#wrapper'), true)
					imgUrl = $this.data 'url-img'
					htmlPreview = []
					if typeof(imgUrl) != undefined && imgUrl != ""
						htmlPreview.push('<img src="'+imgUrl+'" height="160px">')
					else
						$this = $(this)
						$this.attr 'required', 'required'
						$this.attr 'data-parsley-errors-container',"#mainImg"
					$this.fileinput
						allowedFileExtensions : ['jpg', 'gif', 'png', 'jpeg']
						initialPreview: htmlPreview
						showUpload: false
						dropZoneEnabled: true
					utils.loader($('#wrapper'), false)
					$(st.mainImg).on 'filecleared', ()->
						$this = $(this)
						$this.attr 'required', 'required'
						$this.attr 'data-parsley-errors-container',"#mainImg"
				return
			return
		dynamicInputs: ()->
			dom.select.on 'change', ()->
				$this = $(this)
				functions.initialPreview()
				return
			return
		firstChange: ()->
			if dom.select.val() == '3'
				utils.block dom.ctn, false
				functions.showMultipleInputs()
			else if dom.select.val() == '1'
				utils.block dom.ctn, true
			else
				utils.block dom.ctn, false
				$.when(functions.removeOthers()).then ()->
					functions.initialPreview
					functions.showSingleInputs
			return
		removeOthers: ()->
			$('.file-preview-frame').eq(2).find('.kv-file-remove').trigger 'click'
			$('.file-preview-frame').eq(3).find('.kv-file-remove').trigger 'click'
			$('.file-preview-frame').eq(4).find('.kv-file-remove').trigger 'click'
			return
		initialPreview: ()->
			id= $("input[name='id']").val()
			utils.loader $('.container_form'), true
			$.ajax
				url: "/admpanel/provider/image-provider-premium/"+id
				success: (data)->
					fileInputPreview.html = []
					fileInputPreview.config = []
					for a,b in data.data
						objData= {}
						objData['caption'] = a.name
						objData['url'] = '/admpanel/provider/delete-image-provider-premium'
						objData['key'] = a.id
						objData['width'] = "120px"
						objData['extra'] = ()->
							$('.kv-file-remove').unbind()
						fileInputPreview.html.push '<img src="'+a.url+'" title="'+a.name_picture+'" class="file-preview-image">'
						fileInputPreview.config.push objData
					functions.firstChange()
					utils.loader $('.container_form'), false
					return
			return
		showMultipleInputs: ()->
			dom.ctn.html $(st.tpl).html()
			$('.img-ctn .js-fileinput').fileinput
				allowedFileExtensions : ['jpg', 'gif', 'png', 'jpeg']
				initialPreview: fileInputPreview.html
				initialPreviewConfig: fileInputPreview.config
				uploadUrl: '/admpanel/provider/image-provider-premium'
				uploadAsync: true
				minFileCount: 1
				maxFileCount: 4
				overwriteInitial: false
				uploadExtraData:
					pr_provider_id: $("[name='id']").val()
				utils.loader($('#wrapper'), false)
			return
		showSingleInputs: ()->
			dom.ctn.html $(st.tpl).html()
			$('.img-ctn .js-fileinput').fileinput
				allowedFileExtensions : ['jpg', 'gif', 'png', 'jpeg']
				initialPreview: fileInputPreview.html
				initialPreviewConfig: fileInputPreview.config
				uploadUrl: '/admpanel/provider/image-provider-premium'
				uploadAsync: true
				minFileCount: 1
				maxFileCount: 1
				uploadExtraData:
					pr_provider_id: $("[name='id']").val()
			utils.loader($('#wrapper'), false)
			return
	functions.bindSimple() #bind simple imgs
	if $('.img-ctn').length > 0
		functions.initialPreview()
	functions.dynamicInputs()
	return
)