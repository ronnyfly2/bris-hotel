define [], () ->
	console.log 'changeFormAuth'
	st =
		link : 'form > a'
	dom = {}
	catchDom = ->
		dom.link = $(st.link)
		return
	suscribeEvents = ->
		dom.link.on 'click', events.changeFormOne
		return
	events =
		changeFormOne : (e)->
			$('.alert_message').removeClass('actived')
			$('form').toggleClass 'show'
			return
	catchDom()
	suscribeEvents()
	return